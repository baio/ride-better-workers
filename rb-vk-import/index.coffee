"use strict"

Promise = require "bluebird"
feed = Promise.promisifyAll require "feed-read"
cheerio = require "cheerio"
storage = require "./storage"
tt = require "rb-tiny-helpers/transloadit-import-image"
log = require "rb-tiny-helpers/log"


tt.ini { 
  TRANSLOADIT_KEY : process.env.TRANSLOADIT_KEY,  
  TRANSLOADIT_SECRET : process.env.TRANSLOADIT_SECRET 
  TRANSLOADIT_TEMPLATE_ID : process.env.TRANSLOADIT_TEMPLATE_ID
}

log.init __dirname, process.env.LOGGLY_KEY, ["worker", "import"]

mapFeedItem = (item) ->
  date : new Date item.published
  author : item.author
  description : item.content
  link : item.link  
  group : 
    link : item.feed.link
    name : item.feed.name   

mapItem = (item) ->
  descr = item.description
  if descr and item.group.name == item.author
    $ = cheerio.load "<body>" + item.description.replace("<br>", "\n") + "</body>"
    item.text = $("body").text().replace(/^\n+/, "").replace(/\n+$/, "")
    item.img = $("img")[0]?.attribs["src"]
    item

readFeed = (name) ->  
  feed.getAsync("http://feed.exileed.com/vk/feed/#{name}/?only_admin=1&count=5")
  .error (err) ->
    log.write oper : "read_src", status : "error", error : err, src : name
    Promise.resolve()


importFeed = (spot, name) ->
  log.write oper : "import", status : "start", src : name
  readFeed(name)
  .then (res) ->
    res ?= []
    res.map(mapFeedItem).map(mapItem).filter (f) -> f
  .each (res) ->
    storage.get(res.link).then (doc) ->
      if !doc
        res._oper = "create" 
      else if doc.data.text != res.text
        res._docId = doc._id      
        res._oper = "update" 
      else      
        res._oper = "skip" 
      res
  .each (res) ->
    if res._oper == "create" and res.img
      tt.importImg(res.img, "board").then (img) ->
        res._imgSrc = img.src
  .each (res) ->
    if res._oper == "create"
      importOpts = 
        from : "vk"
        link : res.link
        date : res.date
        user_name : res.author
        group_link : res.group    
      storage.insert spot, res, res._imgSrc, importOpts
    else if res._oper == "update"
      storage.update res._id, res.text
  .then (res) ->    
    res = res.map (m) ->
      group : m.group.link
      link : m.link
      oper : m._oper
    log.write oper : "import", status : "success", items : res


doc2src = (doc) ->
  spot : doc._id
  name : doc.contacts.filter((f) -> f.type == "vk")[0].val.replace(/^https?:\/\/vk\.com\//, "")
    
importFeeds = ->

  next = (doc) ->
    src = doc2src(doc)
    importFeed(src.spot, src.name)

  storage.read next
  
  
log.write oper : "app_start", status : "success"

importFeeds().then ->
  log.write oper : "app_stop", status : "success"
, (err) ->
  log.write(oper : "app_stop", status : "error", error : err)
