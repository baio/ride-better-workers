mongo = require "rb-tiny-helpers/mongo"


mongo.ini(process.env.MONGO_URI, ["ths", "resorts", "spots"])

exports.read = (next) ->
  mongo.read "resorts", next, {"contacts.type" : "vk"}

mapItem = (spot, item, imgSrc, importOpts) ->
  user :
    key : "importer"
    name : importOpts.user_name
  created : importOpts.date
  tags : [spot, "message"]
  data : 
    text : item.text
    img : imgSrc
    meta : priority : "important"
  import : importOpts

exports.insert = (spot, item, imgSrc, importOpts) ->
  doc = mapItem spot, item, imgSrc, importOpts
  mongo.resorts.findOneAsync({_id : spot}, {title : 1}).then (res) ->
    doc.spot = res
    mongo.ths.insertAsync doc

exports.update = (id, text) ->
  mongo.ths.updateAsync {_id : mongo.ObjectId(id)}, {$set : "data.text" : text}

exports.get = (link) ->
  mongo.ths.findOneAsync({"import.link" : link}, {"data.text" : 1})