loggly = require 'loggly'
Promise = require "bluebird"

client = Promise.promisifyAll loggly.createClient
  token: process.env.LOGGLY_KEY,
  subdomain: "baio",
  tags: ["ride-better", "worker", "forecastio", "grabber", process.env.APP_NAME],
  json:true


exports.write = (msg) ->
  console.log msg
  client.logAsync msg
