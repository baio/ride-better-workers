mongojs = require "mongojs"
Promise = require "bluebird"
moment = require "moment"

db = mongojs(process.env.MONGO_URI, ["fhist"])
dblocs = mongojs(process.env.MONGO_URI_LOCATIONS, ["resorts"])
skimap = Promise.promisifyAll dblocs.resorts
fhist = Promise.promisifyAll db.fhist

exports.findLocations = (skip, limit) ->
  cursor = Promise.promisifyAll skimap.find({geo : $exists : 1}, {geo : 1, id : 1})
  cursor.sort(_id : 1)
  if skip
    cursor.skip(skip)
  if limit
    cursor.limit(limit)
  cursor

exports.nextLocation = (cursor) ->
  cursor.nextAsync()

exports.writeForecastHistory = (doc) ->
  fhist.insertAsync(doc)


