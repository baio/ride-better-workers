moment = require "moment"
mongo = require "./mongo"
forecastio = require "./forecastio"
log = require "./log"
Promise = require "bluebird"

###
  APP_NAME
  MONGO_URI_LOCATIONS
  MONGO_URI_LOCATIONS
  FORECASTIO_HIST_PAGE (opt)
  FORECASTIO_KEY
  LOGGLY_KEY
  REQUEST_DELAY=100
  REQUEST_TIMEOUT=500 (opt)
  FAILS_EXIT_COUNT=10 
###

pageNumber = parseInt process.env.FORECASTIO_HIST_PAGE
requestDelay = parseInt process.env.REQUEST_DELAY || "100"
requestTimeout = parseInt process.env.REQUEST_TIMEOUT || "100"
failsExitCount = parseInt process.env.FAILS_EXIT_COUNT || "10"
pageSize = 950


failsCount = 0

readNext = (date, cursor) ->
  mongo.nextLocation(cursor).then (doc) ->
    if doc
      Promise.delay(requestDelay)
      .then ->
        forecastio.getForecast(date, doc)
      .then -> readNext(date, cursor)
      .catch (err) ->
        failsCount++
        log.write(oper : "app_error", status : "error", error : err, failsCount : failsCount)
        if failsCount < failsExitCount
          readNext(date, cursor)
        else
          cursor.destroy()
          throw err
    else
      cursor.destroy()
      null

grabLastDay = ->
  #return new Promise (done) -> done()
  skip = pageSize * (pageNumber - 1) if pageNumber
  limit = pageSize if pageNumber
  date = moment.utc().startOf("d").add(-12, "h")
  dateUnix = date.unix()
  log.write(oper : "grab_opts_info", date : date.toDate(), skip : skip, limit : limit)
  readNext dateUnix, mongo.findLocations(skip, limit)

opts = 
  pageNumber : pageNumber
  requestDelay : requestDelay 
  requestTimeout : requestTimeout 
  failsExitCount : failsExitCount 

log.write oper : "app_start", status : "success", opts : opts

grabLastDay()
.then ->
    log.write(oper : "app_stop", status : "success")
    .then ->
        process.exit 0
.catch (err) ->
  log.write(oper : "app_stop", status : "error", error : err)
  .then ->
    process.exit 0


