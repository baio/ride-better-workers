Forecast = require "forecast.io"
Promise = require "bluebird"
forecast = Promise.promisifyAll new Forecast APIKey: process.env.FORECASTIO_KEY
moment = require "moment"

exports.getForecast = (date, doc) ->
  exclude = "currently,minutely,hourly,alerts,flags"
  formattedDate = moment.utc(date).format("YYYY-MM-DDTHH:mm:SSZ");
  forecast.getAtTimeAsync(doc.geo[0], doc.geo[1], formattedDate, {units : "si", exclude : exclude})
  .then (res) ->
    if res and res[1] and res[1].daily
      return res[1].daily.data[0]
    else
      throw new Error "Forecast.io return no results [#{res[1]}]"

