var from = 1417501820;
var to = 1418106620;

db.fhist.mapReduce(

        function () {

            var date = new Date(this.time * 1000);

            var dateUTC = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate(), 0, 0, 0, 0));

            var unix = dateUTC.getTime() / 1000;

            return emit(this.spot, {dates: [unix], amounts: [
                { type: this.precipType, val: this.precipAccumulation}
            ]});

        },

        function (key, values) {

            var res = {dates: [], amounts: []};

            for (var i = 0; i < values.length; i++) {

                var val = values[i];

                for (var k = 0; k < val.dates.length; k++) {

                    if (res.dates.indexOf(val.dates[k]) === -1) {

                        res.dates.push(val.dates[k]);

                        res.amounts.push(val.amounts[k]);

                    }

                }

            }


            return res;

        },

        {

            query : {time : { $gte : from, $lte : to }},

            out : {"replace" : "fhist_reduced_t" },

            finalize: function (key, reducedValue) {

                var res = {amounts: []};

                for (var k = 0; k < reducedValue.dates.length; k++) {

                    var date = NumberLong(reducedValue.dates[k]);

                    var type = reducedValue.amounts[k].type;

                    var amount = reducedValue.amounts[k].val;

                    res.amounts.push({

                        date: date,

                        type: type,

                        amount: amount

                    });

                }

                res.amounts.sort(function(a, b){
                    return a.date > b.date ? 1 :  -1;
                })

                for (var i = 0; i < res.amounts.length; i++)
                {
                    res.amounts[i].cumulSnowAmount = 0;
                    
                    for (var k = 0; k <= i; k++)
                    {
                        if (res.amounts[k].type == "snow")
                        {
                            res.amounts[i].cumulSnowAmount += res.amounts[k].amount;
                        }
                    }                
                }

                return res;

            }

        }

    )