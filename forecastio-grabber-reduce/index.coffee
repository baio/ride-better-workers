moment = require "moment"
mongo = require "./mongo"
log = require "./log"

###
APP_NAME
MONGO_URI
LOGGLY_KEY
###

mapReduce = ->
  yesterday = moment.utc().endOf("d").add(-1, "d")
  opts =
    from : yesterday.add(-6, "d").toDate()
    to : yesterday.toDate()
  mongo.mapReduceHist opts

log.write oper : "app_start", status : "success"

promise = mapReduce()
.then ->
    log.write(oper : "app_stop", status : "success")
    .then ->
        process.exit 0
.catch (err) ->
    log.write(oper : "app_stop", status : "error", error : err)
    .then ->
      process.exit 1




