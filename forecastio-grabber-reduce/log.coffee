loggly = require 'loggly'
Promise = require "bluebird"
pack = require "./package"

client = Promise.promisifyAll loggly.createClient
  token: process.env.LOGGLY_KEY,
  subdomain: "baio",
  tags: ["ride-better", "worker", "forecastio", "reducer", pack.name, pack.version],
  json:true


exports.write = (msg) ->
  console.log msg
  client.logAsync msg
