mongojs = require "mongojs"
Promise = require "bluebird"
moment = require "moment"
fhistMapReduce = require "./fhist-mr"

db = mongojs(process.env.MONGO_URI, ["fhist"])
fhist = Promise.promisifyAll db.fhist

exports.mapReduceHist = (opts) ->

  dateStart = moment.utc(opts.from).startOf("d").unix()
  dateEnd = moment.utc(opts.end).endOf("d").unix()

  fhistMapReduce fhist, dateStart, dateEnd



